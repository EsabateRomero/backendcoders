package api.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import api.seguridad.entity.Usuario;

@Entity
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String contenido;
    
    @ManyToOne()
    @JoinColumn(name = "usuario")
    private Usuario usuario;

    public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Post() {
    }

    public Post(String contenido, Usuario usuario) {
        this.contenido = contenido;
        this.usuario = usuario;
    }

    public Post(String contenido) {
        this.contenido = contenido;
       
    }

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

    
}
