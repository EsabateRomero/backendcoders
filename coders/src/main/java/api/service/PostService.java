package api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import api.entity.Post;
import api.repositorio.PostRepository;
import api.seguridad.entity.Usuario;

@Service
@Transactional
public class PostService {

	@Autowired
	PostRepository postRepositorio;
	
	public List<Post> listar(){
		return postRepositorio.findAll();
	}
	
	public List<String> listarPorUsuario(Usuario usuario){	

		return postRepositorio.findByUsuario(usuario);
	}
	
	public void save(Post post){
	    postRepositorio.save(post);
	}
	
	public List<String> mostrarPosts(){
		return postRepositorio.mostrarPosts();
	}
	
}
