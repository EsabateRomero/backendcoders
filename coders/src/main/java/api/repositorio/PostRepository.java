package api.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import api.dto.PostDto;
import api.entity.Post;
import api.seguridad.entity.Usuario;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
  	
	@Query("SELECT contenido FROM Post WHERE usuario = :usuario")
	List<String> findByUsuario(@Param("usuario") Usuario usuario);
	
	@Query("SELECT contenido FROM Post")
	List<String> mostrarPosts();
}
