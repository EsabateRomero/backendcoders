package api.dto;

import java.util.Calendar;

public class PostDto {

    private String contenido;

    public PostDto() {
    }

    public PostDto(String contenido) {
        this.contenido = contenido;
    }

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}



}
