package api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;

import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import api.dto.Mensaje;
import api.dto.PostDto;
import api.entity.Post;
import api.seguridad.dto.JwtDto;
import api.seguridad.dto.UsuarioPrincipal;
import api.seguridad.entity.Usuario;
import api.seguridad.jwt.JwtProvider;
import api.seguridad.jwt.JwtTokenFilter;
import api.seguridad.service.UsuarioService;
import api.service.PostService;
import javassist.expr.NewArray;

@RestController
@RequestMapping("/posts")
@CrossOrigin(origins = "*")
public class PostController {
	
	@Autowired
	PostService postService;
	
	@Autowired 
	UsuarioService usuarioService;

	@Autowired
	JwtProvider jwtProvider;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/lista")
	public ResponseEntity<List<String>> list(){
		List<String> list = postService.mostrarPosts();
		 if(list.size()==0)
	        	return new ResponseEntity(new Mensaje("No hay posts"), HttpStatus.OK);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	
	@GetMapping("/muro")
	public ResponseEntity<String> getByUsuario(@RequestHeader(name="Authorization") String req){
	
		req.toString().replace("Bearer ", "");
		 String nombreUsuario = jwtProvider.getNombreUsuarioFromToken(req);
		Usuario usuario = usuarioService.getByNombreUsuario(nombreUsuario).get();
	
		List<String> listPostUsers = postService.listarPorUsuario(usuario);
		
	    if(listPostUsers.size()==0)
        	return new ResponseEntity(new Mensaje("No hay posts"), HttpStatus.OK);
        return new ResponseEntity(listPostUsers, HttpStatus.OK);
        
	}
	
	@PostMapping("/crear")
	public ResponseEntity<?> crear(@RequestBody PostDto postDto, @RequestHeader(name="Authorization") String req){
			
		req.toString().replace("Bearer ", "");
		 String nombreUsuario = jwtProvider.getNombreUsuarioFromToken(req);
		Usuario usuario = usuarioService.getByNombreUsuario(nombreUsuario).get();
		
		if(postDto.getContenido().equals(null))
			return new ResponseEntity(new Mensaje("el contenido del post no puede estar vacío"), HttpStatus.BAD_REQUEST);
		
		Post post = new Post(postDto.getContenido(), usuario); 
		postService.save(post);
		return new ResponseEntity(new Mensaje("post creado"), HttpStatus.OK);
	}



}
