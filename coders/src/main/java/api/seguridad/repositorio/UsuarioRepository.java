package api.seguridad.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import api.seguridad.entity.Usuario;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    Optional<Usuario> findByNombreUsuario(String nombreUsuario);
    boolean existsByNombreUsuario(String nombreUsuario);
    boolean existsByEmail(String email);
    
    
    /*
     * implementar: ver usuarios connectados /// método solo para administradores
     * He encontrado este código pero falta comprobar y considero que falta
     * implementar en el usuario status 1. 
     */
    //@Query("SELECT u FROM User u WHERE u.status = 1")
    //Collection<Usuario> findAllActiveUsers();*/

}
